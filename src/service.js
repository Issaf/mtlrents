
export {
  currency
}

function currency(nbr) {
  return "$" + nbr.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}