# MTLRents
### PS: Still needs performance improvements

Small project that estimates rent prices given an address, number of bedrooms and bathrooms. It was built with Vue.js and Material Design framework Vuetify.js. The algoritm is in a [different repository](https://gitlab.com/Issaf/mtlrentsml).